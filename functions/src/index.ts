import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as bodyParser from 'body-parser';
import * as express from 'express';
//import { user } from 'firebase-functions/lib/providers/auth';



//import { user } from 'firebase-functions/lib/providers/auth';

//Firestore references
admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

//Express instances
const main = express();
const app = express();



main.use('/api/v1', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({extended: false}));


export const webApi = functions.https.onRequest(main);

app.get('/getreports', async (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const userQuerySnapshot  = await db.collection("reportes").get();
    const reports: any[] = [];

    userQuerySnapshot.forEach(
        (doc) => {
        reports.push({
            data: doc.data()

        });
        }
    
    );
    res.status(200).json(reports);
   }
   catch(error){
    res.status(500).send(error);
   }

});

//DUDA
app.get('/getreportsData', async (req, res) => {
    const reports: any[] = [];

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const departamento= req.query.departamento || '';
    const area = req.query.area || '' ;
    const tipo = req.query.tipo || '';

    const reportsRef  = await db.collection("reportes")
    let query;
    if(departamento !== '' && area  !== '' && tipo !== ''){
        query = reportsRef.where('departamento','==', departamento)
        .where('area','==',area)
        .where('tipo','==',tipo).orderBy('fecha').limit(10);
        console.log("todos");
    }
    else if(departamento  !== ''&& area !== ''){
        query = reportsRef.where('departamento','==', departamento)
        .where('area','==',area).limit(10);
        console.log("dos");
    }
    else if(departamento !== ''){
        query = reportsRef.where('departamento','==', departamento).limit(10);
        console.log("depa");
    }
    else if(area !== ''){
        query = reportsRef.where('area','==',area).limit(10);
        console.log("area");
    }
    else{
        console.log("nada");
        query = reportsRef.where('campoficticio','==',"").limit(1);
    }
    
    
    query.get().then(snapshot => {
        if(snapshot.empty){
            res.status(200).send({});
        }

        snapshot.forEach(doc => {
            reports.push({id: doc.id, data:doc.data()});
        });
        res.status(200).send(reports);
    }).catch(error => {
        res.status(500).send(error);
    })

   }
   catch(error){
    res.status(500).send(error);
   }

});

app.get('/getreportsDataList', async (req, res) => {
    console.log("DENTRO");
    const reports: any[] = [];

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const departamento= req.query.departamento || '';
    const area = req.query.area || '' ;
    const tipo = req.query.tipo || '';
    const index: any= req.query.index || 0;

    const reportsRef  = await db.collection("reportes")
    let query;
    if(departamento !== '' && area  !== '' && tipo !== ''){
        query = reportsRef.where('departamento','==', departamento)
        .where('area','==',area)
        .where('tipo','==',tipo).orderBy('fecha').orderBy('fecha').limit(10).offset(index*10);
        console.log("todos");
    }
    else if(tipo  !== ''&& area !== ''){
        query = reportsRef.where('tipo','==', tipo)
        .where('area','==',area).orderBy('fecha').limit(10).offset(index*10);
        console.log("dos");
    }
    else if(departamento  !== ''&& area !== ''){
        query = reportsRef.where('departamento','==', departamento)
        .where('area','==',area).orderBy('fecha').limit(10).offset(index*10);
        console.log("dos");
    }
    else if(departamento !== ''){
        query = reportsRef.where('departamento','==', departamento).orderBy('fecha').limit(10).offset(index*10);
        console.log("depa");
    }
    else if(area !== ''){
        query = reportsRef.where('area','==',area).orderBy('fecha').limit(10).offset(index*10);
        console.log("area");
    }
    else{
        console.log("nada");
        query = reportsRef.orderBy('fecha').limit(10).offset(index*10);
    }
    
    console.log("MITAD");
    query.get().then(snapshot => {
        console.log("MITAD1");
        if(snapshot.empty){
            res.status(200).send();
        }

        snapshot.forEach(doc => {
            reports.push({
                id: doc.id, 
                titulo: doc.get('titulo'),
                fecha: doc.get('fecha'),
                area: doc.get('area'),
                tipo: doc.get('tipo')
            });
        });
        res.status(200).send(reports);
    }).catch(error => {
        res.status(500).send('ERROR2'+ error);
    })

   }
   catch(error){
    res.status(500).send("EEROR1");
   }

});

app.get('/getreportData', async (req, res) => {
    

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const id :string= req.query.id.toString() || '';
 

    const reportsRef  = await db.collection("reportes").doc(id);
    reportsRef.get().then(
        doc =>{
            if(doc.exists){
                res.status(200).send(doc.data());   
            }
            else{
                res.status(404).send('No encontrado');
            }
        }
    ).catch(
        error=>{
            res.status(500).send(error);
        }
    );
   }
   catch(error){
    res.status(500).send(error);
   }

});

app.get('/getRol', async (req, res) => {
    

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const id :string= req.query.id.toString() || '';
 

    const reportsRef  = await db.collection("usuariosweb").doc(id);
    reportsRef.get().then(
        doc =>{
            if(doc.exists){
                res.status(200).send(doc.get("rol"));   
            }
            else{
                res.status(404).send('No encontrado');
            }
        }
    ).catch(
        error=>{
            res.status(500).send(error);
        }
    );
   }
   catch(error){
    res.status(500).send(error);
   }

});

app.get('/getreporttomap', async (req, res) => {
    const reports: any[] = [];

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const departamento= req.query.departamento || '';
    const area = req.query.area || '' ;
    const tipo = req.query.tipo || '';

    const reportsRef  = await db.collection("reportes")
    let query;
    if(departamento !== '' && area  !== '' && tipo !== ''){
        query = reportsRef.where('departamento','==', departamento)
        .where('area','==',area)
        .where('tipo','==',tipo);
        console.log("todos");
    }
    else if(departamento  !== ''&& area !== ''){
        query = reportsRef.where('departamento','==', departamento)
        .where('area','==',area);
        console.log("dos");
    }
    else if(tipo  !== ''&& area !== ''){
        query = reportsRef.where('tipo','==', tipo)
        .where('area','==',area);
        console.log("dos");
    }
    else if(departamento !== ''){
        query = reportsRef.where('departamento','==', departamento);
        console.log("depa");
    }
    else if(area !== ''){
        query = reportsRef.where('area','==',area);
        console.log("area");
    }
    else{
        console.log("nada");
        query = reportsRef
    }
    
    
    query.get().then(snapshot => {
        if(snapshot.empty){
            res.status(200).send();
        }

        snapshot.forEach(doc => {  
            reports.push({ubicacion: doc.get('ubicacion'), id: doc.id, titulo: doc.get('titulo') });
        });
        res.status(200).send(reports);
    }).catch(error => {
        res.status(500).send(error);
    })

   }
   catch(error){
    res.status(500).send(error);
   }

});

app.post('/savereport',async (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'POST');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }

    
    try{
        if( req.body['titulo']!=='' && req.body['area']!=='' && req.body['area']!=='' && req.body['departamento']!==''&& req.body['descripcion']!==''&& req.body['latitude']!==''&& req.body['longitude']!==''){
            await db.collection('reportes').add({
                titulo: req.body['titulo'],
                area: req.body['area'],
                tipo: req.body['tipo'],
                departamento: req.body['departamento'],
                descripcion: req.body['descripcion'],
                fecha: admin.firestore.Timestamp.now(),
                fotografia: '',
                ubicacion: new admin.firestore.GeoPoint(parseFloat(req.body['latitude']), parseFloat(req.body['longitude'])),
    
    
            });
            res.status(200).send('Registro Guardado con éxito');
        }
        else{
            res.status(200).send('Faltan campos del reporte');
        }
        
       
    }
    catch(error){
        res.status(500).send(error);
    }
});

app.get('/getAreasData', async (req, res) =>{
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'GET');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }


    try{
        const docAreaRef = await db.collection('data').doc('areas_tipo')
        
        docAreaRef.get().then((doc: any)=>{
            if(doc.exists){
                const data = doc.data();
                const finalData = Object.keys(data).map(key => ({area: key, tipos: data[key]}));
                res.status(200).send(finalData);
            }
            else {
                // doc.data() will be undefined in this case
                res.status(404).send("No such document!");
            }
        }).catch((error: any)=>{
            res.status(500).send(error);
        });
        
    }
    catch(error){
        res.status(500).send(error);
        
    }
});

app.get('/getDepartmentData', async (req, res) =>{
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'GET');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }


    try{
        const docAreaRef = await db.collection('data').doc('departamentos')
        
        docAreaRef.get().then((doc: any)=>{
            if(doc.exists){
                res.status(200).send(doc.get('departamentos'));
            }
            else {
                // doc.data() will be undefined in this case
                res.status(404).send("No such document!");
            }
        }).catch((error: any)=>{
            res.status(500).send(error);
        });
        
    }
    catch(error){
        res.status(500).send(error);
        
    }
});


app.get('/disableUser', async(req, res) => {
 
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set	('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }

    try{
        const uid:string = req.query.id.toString();

        if(uid!==""){
            admin.auth().updateUser(uid,{
                disabled: true
            })
            .then(userRecord =>{
                res.send({messsage:"Actualizado"});
            })
            .catch(error => {
                res.send({message: "Error al actualizar"});
            });
        }
        else{
            res.status(200).send({message: "usuario desconocido"});
        }
    }
    catch(error){
        console.log("ERROR AL MODIFICAR USUARIO:" + error);
    }
    
});

app.get('/enableUser', async(req, res) => {
 
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set	('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }

    try{
        const uid:string = req.query.id.toString();

        if(uid!==""){
            admin.auth().updateUser(uid,{
                disabled: false
            })
            .then(userRecord =>{
                res.send({messsage:"Actualizado"});
            })
            .catch(error => {
                res.send({message: "Error al actualizar"});
            });
        }
        else{
            res.status(200).send({message: "usuario desconocido"});
        }

    }
    catch(error){
        console.log("ERROR AL MODIFICAR USUARIO:" + error);
    }
    
});
//---------Para graficas
app.get('/getChartsData', async (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const counters  = await db.collection("data").doc('contadores').get();

   if(counters.exists){
        let departamentos :any[]= counters.get('reportes.departamentos');
        let departamento_value: any[]=[];

       for(let departamento in departamentos){
           departamento_value.push({name: departamento,value:departamentos[departamento].total });
       }

        
        res.status(200).json(departamento_value);
    }
    else{
        res.status(200).send('NADA');
    }
   }
   catch(error){
    res.status(500).send(error);
   }

});


app.get('/getChartsDataDepartaments', async (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const counters  = await db.collection("data").doc('contadores').get();

   if(counters.exists){
        let departamentos :any[]= counters.get('reportes.departamentos');
        let departamento_value: any[]=[];
        

       for(let departamento in departamentos){
        let areas_values: any[] = [];
        for(let area in departamentos[departamento].areas){
            areas_values.push(
               {name: area, value: departamentos[departamento].areas[area].total}
            );
        }

           departamento_value.push({name: departamento,value:areas_values});
       }

        
        res.status(200).json(departamento_value);
    }
    else{
        res.status(200).send('NADA');
    }
   }
   catch(error){
    res.status(500).send(error);
   }

});


app.get('/getChartsDataAreas', async (req, res) => {

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
    const counters  = await db.collection("data").doc('contadores').get();

   if(counters.exists){
        let departamentos :any[]= counters.get('reportes.departamentos');
        let areasData: any[]=[];
        let areas: any[]=[];

      
        

       for(let departamento in departamentos){
        for(let area in departamentos[departamento].areas)
            if(!areas.includes(area)){
                areas.push(area);
            }
          
       }
       
       for(let i=0;i<areas.length; i++){
           let departamentsValues: any[] = []
        for(let departamento in departamentos){
            if(departamentos[departamento].areas[areas[i]]){
                departamentsValues.push({name: departamento, value: departamentos[departamento].areas[areas[i]].total}) 
            }

            
        }
        areasData.push({name:areas[i], value: departamentsValues });
            
          
       }

        res.status(200).json(areasData);
    }
    else{
        res.status(200).send('NADA');
    }
   }
   catch(error){
    res.status(500).send(error);
   }

});

//----------------------------------------------------

export const createReportUpdateAreaCounter = functions.firestore
    .document('reportes/{reportId}')
    .onCreate((snap, context) => {
    
        const report = snap.data();
        if(report && !report.empty){
            const area = report.area;
            const tipo = report.tipo;
            const departamento = report.departamento;

            const tipoActualizar =  "reportes.departamentos."+departamento+".areas."+area+".tipos."+tipo;
            const areaActualizar =  "reportes.departamentos."+departamento+".areas."+area+".total";
            const departamentoActualizar = "reportes.departamentos."+departamento+".total";
            const nacionalActualizar = "total";

            const contadores = db.collection('data').doc('contadores');
            contadores.update(tipoActualizar,admin.firestore.FieldValue.increment(1)).then(result=>{
                console.log(result);
            }).catch(error=>{
                console.log(error);
            });
            contadores.update(areaActualizar,admin.firestore.FieldValue.increment(1)).then(result=>{
                console.log(result);
            }).catch(error=>{
                console.log(error);
            });
            contadores.update(departamentoActualizar,admin.firestore.FieldValue.increment(1)).then(result=>{
                console.log(result);
            }).catch(error=>{
                console.log(error);
            });
            contadores.update(nacionalActualizar,admin.firestore.FieldValue.increment(1)).then(result=>{
                console.log(result);
            }).catch(error=>{
                console.log(error);
            });
        }
        
    });
    
app.get('/getUserList', async (req, res) =>{


    res.set('Access-Control-Allow-Origin', '*');
    
    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'GET');
      res.set('Access-Control-Allow-Headers', '*');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }
    const userslist: any[] =[];
    const userUidsBD: any[] = [];
    const userPrivileges: any[]= [];

    try{
        const usuariosRef = await db.collection("usuariosweb").get();
        usuariosRef.forEach(
            doc =>{
                if(doc.exists && doc.get("uid")!=null){
                    userUidsBD.push(
                       doc.get("uid")
                    );

                    userPrivileges.push(
                        doc.get("rol").admin
                    );
                }
               
            }
        );

        const a = await admin.auth().listUsers();
        a.users.forEach(
            user =>{
                let index = userUidsBD.indexOf(user.uid);
               if(index>-1 && !userPrivileges[index]){
                userslist.push({
                    uid: user.uid,
                    habilitado: user.disabled,
                    nombre: user.displayName,
                   
                    verificado: user.emailVerified,
                    email: user.email

                })
               }
            }
        );

       

      

        res.status(200).send(userslist);
    }
    catch(error){
        res.status(500).send(error);
    }

}
);







//EXTERNAL ACCESS RECOMMENDATIONS

//saveRecomendation
app.post('/saveRecommendation',async (req, res) => {
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') {
      // Send response to OPTIONS requests
      res.set('Access-Control-Allow-Methods', 'POST');
      res.set('Access-Control-Allow-Headers', 'Content-Type');
      res.set('Access-Control-Max-Age', '3600');
      res.status(204).send('');
    }

    
    try{
        if( req.body['titulo']!=='' && req.body['descripcion']!=='' && req.body['id']!==''){
            await db.collection('sugerencias').add({
                titulo: req.body['titulo'],
                descripcion: req.body['descripcion'],
                fecha: admin.firestore.Timestamp.now(),
                id:  req.body['id']
    
            });
            res.status(200).send('Sugerencia guardada con éxito');
        }
        else{
            res.status(200).send('Faltan campos de la sugerencia');
        }
        
       
    }
    catch(error){
        res.status(500).send(error);
    }
});
  
//getRecomendations
app.get('/getRecommendation', async (req, res) => {
    const recommendations: any[] = [];

    res.set('Access-Control-Allow-Origin', '*');

      if (req.method === 'OPTIONS') {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'GET');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
      }

   try{
 

    const reportsRef  = await db.collection("sugerencias")
    
    const query = reportsRef.where('id','==',req.query.id);
    
    query.get().then(snapshot => {
        if(snapshot.empty){
            res.status(200).send({});
        }

        snapshot.forEach(doc => {
            recommendations.push({data: doc.data()});
        });
        res.status(200).send(recommendations);
    }).catch(error => {
        res.status(500).send(error);
    })

   }
   catch(error){
    res.status(500).send(error);
   }

});